var style = (function() {
    var me = {};

    me.getFontSize = function() {
        return retrieve("font_size", "normal");
    };

    me.setFontSize = function(fontSize) {
        store("font_size", fontSize);
    };

    me.getLayout = function() {
        return retrieve("layout", "list");
    };

    me.setLayout = function(layout) {
        store("layout", layout);
    };

    me.getShowDetails = function() {
        return retrieve("show_details", false);
    };

    me.setShowDetails = function(showDetails) {
        store("show_details", showDetails);
    };

    me.getCssClasses = function() {
        return [
            'layout-' + me.getLayout(),
            'font-' + me.getFontSize()
        ];
    };

    function store(key, value) {
        var data = chameleon.getData() || {};
        data.style = data.style || {};
        data.style[key] = value;
        chameleon.saveData(data);
    }

    function retrieve(key, defaultValue) {
        var data = chameleon.getData() || {};
        if(data.style && data.style[key]) {
            return data.style[key];
        } else {
            return defaultValue;
        }
    }

    return me;
})();