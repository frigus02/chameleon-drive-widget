var drive = (function() {
    // Public
    var me = {};

    me.MY_DRIVE = {
        id: 'root',
        title: 'My Drive',
        mimeType: 'application/vnd.google-apps.folder'
    };

    me.getCurrentFolder = function() {
        var path = me.getCurrentPath();
        return path[path.length - 1];
    };

    me.getCurrentPath = function() {
        var data = chameleon.getData() || {};
        if(!data.path) data.path = [me.MY_DRIVE];

        return $.map(data.path, function(e, i) {
            return new File(e);
        });
    };

    me.getFile = function(id) {
        var cache = chameleon.getLocalData('cache') || {};
        if(cache.items) {
            return new File(cache.items[id]);
        }
    };

    me.getFiles = function() {
        var cache = chameleon.getLocalData('cache') || {};
        if(!cache.items) return [];
        
        var folder = me.getCurrentFolder();
        var files = [];
        for(var id in cache.items) {
            var item = cache.items[id];
            var parentMatch = $.grep(item.parents, function(e, i) {
                return e.id == folder.id || (folder.id == 'root' && e.isRoot);
            }).length > 0;
            
            if(parentMatch && !item.labels.trashed) {
                files.push(new File(item));
            }
        }
        
        files.sort(function(a, b) {
            if(a.isFolder() && !b.isFolder()) {
                return -1;
            } else if(!a.isFolder() && b.isFolder()) {
                return 1;
            } else {
                return a.title < b.title ? -1 : (a.title > b.title ? 1 : 0);
            }
        });
        
        return files;
    };

    me.update = function(callback, pageToken) {
        // Check if current chached data belongs to current account.
        var account = gapi.getAccount();
        var cache = chameleon.getLocalData('cache') || {};
        if(cache.account != account.id) {
            cache.account = account.id;
            delete cache.nextChangeId;
            delete cache.items;
            chameleon.saveLocalData('cache', cache);
        }

        new gapi.Request({
            path: '/drive/v2/changes',
            method: 'GET',
            params: {
                includeDeleted: 'true',
                includeSubscribed: 'false',
                pageToken: pageToken,
                startChangeId: cache.nextChangeId,
                fields: 'items(deleted,file,fileId),largestChangeId,nextPageToken',
                key: 'AIzaSyCcT1ynm-jOOeER1Zi8Oo7DEF2lHt6NFTY'
            }
        }).execute(function(success, data) {
            if(success) {
                if(!data.nextPageToken) cache.nextChangeId = data.largestChangeId;
                if(!cache.items) cache.items = {};

                var thumbnailExpirationDate = getNewThumbnailExpirationDate();
                for(var i = 0, item; item = data.items[i]; i++) {
                    if(item.deleted) {
                        delete cache.items[item.fileId];
                    } else {
                        if(item.file.thumbnailLink) {
                            item.file.thumbnailLinkExpires = thumbnailExpirationDate;
                        }

                        cache.items[item.fileId] = item.file;
                    }
                }
                
                chameleon.saveLocalData('cache', cache);
                
                if(data.nextPageToken) {
                    me.update(callback, data.nextPageToken);
                } else {
                    if(callback) callback(true);
                }
            } else {
                if(callback) callback(false);
            }
        });
    };

    me.updateThumbnails = function(callback) {
        // Check if thumbnail links have expired.
        var files = me.getFiles();
        var expired = false;
        for(var i = 0, file; file = files[i]; i++) {
            if(file.thumbnailLink && file.thumbnailLinkExpires < new Date().getTime()) {
                expired = true;
                break;
            }
        }
        if(!expired) {
            callback(true);
            return;
        }
        
        // Get new thumbnail links for the items in the current folder.
        var folder = me.getCurrentFolder();
        new gapi.Request({
            path: '/drive/v2/files',
            method: 'GET',
            params: {
                q: "'" + folder.id + "' in parents and trashed = false",
                fields: 'items(id,thumbnailLink)',
                key: 'AIzaSyCcT1ynm-jOOeER1Zi8Oo7DEF2lHt6NFTY'
            }
        }).execute(function(success, data) {
            if(success) {
                var cache = chameleon.getLocalData('cache');
                var expireDate = getNewThumbnailExpirationDate();
                for(var i = 0, item; item = data.items[i]; i++) {
                    cache.items[item.id].thumbnailLink = item.thumbnailLink;
                    cache.items[item.id].thumbnailLinkExpires = expireDate;
                }

                chameleon.saveLocalData('cache', cache);
                callback(true);
            } else {
                callback(false);
            }
        });
    };

    function File(values) {
        $.extend(this, values);

        this.isFolder = function() {
            return this.mimeType == 'application/vnd.google-apps.folder';
        };

        this.isShared = function() {
            return this.userPermission.role != 'owner';
        };

        this.getIcon = function() {
            var icons = {
                'application/vnd.google-apps.document': 'document',
                'application/vnd.google-apps.spreadsheet': 'spreadsheet',
                'application/vnd.google-apps.presentation': 'presentation',
                'application/vnd.google-apps.form': 'form',
                'application/vnd.google-apps.drawing': 'drawing',
                'application/msword': 'word',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.': 'word',
                'application/vnd.ms-excel': 'excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.': 'excel',
                'application/vnd.ms-powerpoint': 'powerpoint',
                'application/vnd.openxmlformats-officedocument.presentationml.': 'powerpoint',
                'application/pdf': 'pdf',
                'image/': 'image',
                'audio/': 'audio',
                'video/': 'video',
                '': 'generic'
            };
            
            if(this.isFolder()) {
                if(this.isShared()) {
                    return 'folder-shared';
                } else {
                    return 'folder';
                }
            } else {
                for(var type in icons) {
                    if(this.mimeType.indexOf(type) == 0) {
                        return icons[type];
                    }
                }
            }
        };

        this.open = function() {
            if(this.isFolder()) {
                openFolder(this);
            } else {
                openFile(this);
            }
        };

        function openFile(file) {
            chameleon.intent({
                action: "android.intent.action.VIEW",
                data: file.alternateLink
            });
        }

        function openFolder(file) {
            var path = me.getCurrentPath();
            for(var i = 0, folder; folder = path[i]; i++) {
                if(folder.id == file.id) {
                    path = path.slice(0, i);
                    break;
                }
            }

            path.push(file);
            
            var data = chameleon.getData() || {};
            data.path = $.map(path, function(e, i) {
                var saveData = {};
                for(var prop in e) {
                    if(typeof e[prop] != 'function') {
                        saveData[prop] = e[prop];
                    }
                }
                return saveData;
            });
            chameleon.saveData(data);
        }
    }

    function getNewThumbnailExpirationDate() {
        return new Date().getTime() + (60 * 60 * 1000);
    }

    return me;
})();