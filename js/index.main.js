$(document).ready(function(){
    var REFRESH_TIMESPAN = 60000 * 10; // Every 10 minutes
    var lastRefresh = null;
    
    chameleon.widget({    
        //Triggered every time the widget loads.
        onLoad: function() {
            chameleon.localization.update();
            drive.MY_DRIVE.title = _('drive_folder_my_drive');
            updateTitle();
            updateStyle();
            startPolling();
        },
        
        //Triggered everytime Chameleon resumes (comes back into focus).                
        onResume: function() {
            chameleon.localization.update();
            drive.MY_DRIVE.title = _('drive_folder_my_drive');
            startPolling();
        },
        
        //Triggered every time Chameleon pauses (goes out of focus).
        onPause: function() {
            stopPolling();
        },
        
        //Triggered when the user enters dashboard edit mode.
        onLayoutModeStart: function() {
            stopPolling();
        },
        
        //Triggered when the user exits dashboard edit mode.
        onLayoutModeComplete: function() {
            startPolling();
        },
        
        //Triggered when the status of network availability changes.
        onConnectionAvailableChanged: function(available) {
            if(available) {
                startPolling();
            } else {
                stopPolling();
            }
        },
        
        //Triggered when the user taps the configure button in the widget title bar.
        onConfigure: function() {
            showSettings();
        },
        
        //Triggered when the user taps the widget titlebar.
        onTitleBar: function() {
            launchDriveApp();
        },
        
        //Triggered when the user taps the refresh button on the widget title bar.
        onRefresh: function() {
            updateData();
        }
    });
    
    //----------------------------------------------------------------------
    // Event handlers
    //----------------------------------------------------------------------
    $('*').live('touchstart', function() {
        $(this).addClass('active');
    }).live('touchend touchmove touchcancel', function() {
        $(this).removeClass('active');
    });
    
    $(window).resize(function() {
        $('#chameleon-widget .file').has('.spinner').spin();
    });
    
    $('.path .up').live('click', function() {
        var path = drive.getCurrentPath();
        path[path.length - 2].open();
        updateView();
    });
    
    $('.files .file').live('click', function() {
        var file = drive.getFile($(this).data('file_id'));
        file.open();
        if(file.isFolder()) {
            updateView();
        }
    });
    
    //----------------------------------------------------------------------
    // Functions
    //----------------------------------------------------------------------
    function updateData() {
        var account = gapi.getAccount();
        if(!chameleon.connected()) {
            if(lastRefresh == null) {
                $('#chameleon-widget').chameleonWidgetNeedWiFiHTML();
            } else {
                // Maybe show small message at the bottom.
            }
        } else if(account === undefined) {
            $('#chameleon-widget').chameleonWidgetConfigureHTML({
                onConfigure: showSettings,
                title: _('drive_configure_prompt_title'),
                caption: _('drive_configure_prompt_caption')
            });
        } else {
            chameleon.showLoading({ showloader: true });
            drive.update(function(success) {
                lastRefresh = new Date().getTime();
                if(success) updateView();
                chameleon.showLoading({ showloader: false });
            });
        }
    }
    
    function updateTitle() {
        var account = gapi.getAccount();
        if(account !== undefined) {
            chameleon.setTitle({ text: account.email });
        } else {
            chameleon.setTitle({ text: "Google Drive" });
        }
    }
    
    function updateStyle() {
        var classes = style.getCssClasses();
        $('#chameleon-widget').attr('class', classes.join(' '));
    }
    
    function updateView() {
        var path = drive.getCurrentPath();
        var $path = $('<div class="path"></div>');
        $path.text(path[path.length - 1].title);
        if(path.length > 1) {
            $path.prepend('<span class="up"></span>');
        }

        var files = drive.getFiles();
        var layout = style.getLayout();
        var showDetails = style.getShowDetails();
        if(files.length > 0) {
            var $files = $('<ul class="files"></ul>');
            for(var i = 0, file; file = files[i]; i++) {
                var $file = $(['<li class="file">',
                    '<div class="infos">',
                        '<span class="name"></span>',
                        '<span class="details"></span>',
                    '</div>',
                '</li>'].join(''));
                $file.find('.name').text(file.title);

                if(showDetails) {
                    var modified = new Date(file.modifiedDate).toLocaleShortDateString();
                    var modifiedBy = file.lastModifyingUserName;
                    $file.find('.details').html(modified + ' &#8226; ' + modifiedBy);
                }
                
                if(layout == 'list') {
                    $file.prepend('<span class="icon ' + file.getIcon() + '"></span>');
                } else if(layout == 'thumbnails') {
                    if(file.isFolder()) {
                        $file.prepend('<img src="images/thumbnail_folder' + (file.isShared() ? '_shared' : '') + '.png" alt="' + file.title + '" class="thumbnail">');
                    } else {
                        $file.spin();
                    }
                }
                
                $file.data('file_id', file.id);
                $files.append($file);
            }
            
            if(layout == 'thumbnails') {
                drive.updateThumbnails(function(success) {
                    var account = gapi.getAccount();

                    $files.children('.file:has(.spinner)').each(function(index, element) {
                        var file = drive.getFile($(element).data('file_id'));
                        if(file.thumbnailLink) {
                            if(file.thumbnailLink.indexOf('https://docs.google.com/feeds/') == 0) {
                                var src = 'services/thumbnail.php?' + $.param({
                                    url: file.thumbnailLink,
                                    token_type: account.token_type,
                                    access_token: account.access_token
                                });
                            } else {
                                var src = file.thumbnailLink;
                            }
                        } else {
                            var src = 'images/thumbnail_generic.png';
                        }
                        
                        var $thumbnail = $('<img src="' + src + '" alt="' + file.title + '" class="thumbnail">').hide();
                        $thumbnail.load(function() {
                            $(element).spin(false);
                            $thumbnail.show();
                        });
                        $(element).prepend($thumbnail);
                    });
                });
            }
        } else {
            var $files = $('<div class="no-files"></div>').chameleonWidgetMessageHTML({
                title: _('drive_folder_empty_title'),
                caption: _('drive_folder_empty_caption'),
                icon: 'images/dust.png'
            });
        }
        
        $('#chameleon-widget').html('').append($path, $files);
        $('#chameleon-widget .file').has('.spinner').spin();
        chameleon.invalidate();
    };
    
    function startPolling() {
        if(lastRefresh == null || new Date().getTime() - lastRefresh >= REFRESH_TIMESPAN) {
            updateData();
        }
        
        if(gapi.getAccount() !== undefined) {
            chameleon.poll({
                id: "refresh_tasks",
                action: "start",
                interval: REFRESH_TIMESPAN,
                callback: updateData
            });
        }
    }
    
    function stopPolling() {
        chameleon.poll({
            id: "refresh_tasks",
            action: "stop"
        });
    }
    
    function launchDriveApp() {    
        // Supported apps
        var components = [{
            package: "com.google.android.apps.docs",
            name: "com.google.android.apps.docs.app.NewMainProxyActivity"
        }];
        
        // Try to start a supported app.
        for(var i = 0; i < components.length; i++) {
            if(chameleon.componentExists(components[i])) {
                chameleon.intent({
                    component: components[i],
                    action: "android.intent.action.MAIN"
                });
                return;
            }
        }
        
        // If no supported app was found, open website.
        chameleon.intent({
            action: "android.intent.action.VIEW",
            data: "https://drive.google.com/"
        });
    }
    
    function showSettings() {
        stopPolling();
        chameleon.promptHTML({
            url: "settings.html",
            callback: function(success, data) {
                if(success) {
                    updateTitle();
                    updateStyle();
                    lastRefresh = null;
                }

                startPolling();
            }
        });
    }
});