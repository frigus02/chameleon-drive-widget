$(document).ready(function(){
    chameleon.localization.update();
    populateStaticTextForLocale();
    
    $('#add-account-button').click(function(e) {
        e.preventDefault();
        gapi.addAccount(populateAccounts);
    });
    
    $('#close-button').click(function(e) {    
        e.preventDefault();
        
        var selectedAccount = $('#account-select').chameleonSelectList({ getSelectedItem: true }).value;
        var selectedLayout = $('#layout-select').chameleonSelectList({ getSelectedItem: true }).value;
        var showDetails = $('#show-details').prop('checked');
        var selectedFontSize = $('#fontsize-select').chameleonSelectList({ getSelectedItem: true }).value;
        
        if(selectedAccount) {
            gapi.getAccount(selectedAccount).setAsCurrent();
        }
        style.setLayout(selectedLayout);
        style.setShowDetails(showDetails);
        style.setFontSize(selectedFontSize);
        chameleon.close(true);
    });
    
    $('#fontsize-select').chameleonSelectList({
        title: _('drive_settings_fontsize_select_title'),
        list: [{
            name: _('drive_settings_fontsize_select_option_xsmall'),
            value: 'xsmall'
        }, {
            name: _('drive_settings_fontsize_select_option_small'),
            value: 'small'
        }, {
            name: _('drive_settings_fontsize_select_option_normal'),
            value: 'normal'
        }, {
            name: _('drive_settings_fontsize_select_option_large'),
            value: 'large'
        }, {
            name: _('drive_settings_fontsize_select_option_xlarge'),
            value: 'xlarge'
        }]
    });
    
    $('#layout-select').chameleonSelectList({
        title: _('drive_settings_layout_select_title'),
        list: [{
            name: _('drive_settings_layout_select_option_list'),
            value: 'list'
        }, {
            name: _('drive_settings_layout_select_option_thumbnails'),
            value: 'thumbnails'
        }]
    });

    $('#account-select').hide();
    $('#layout-select').chameleonSelectList({ selectedValue: style.getLayout() });
    $('#show-details').prop('checked', style.getShowDetails());
    $('#fontsize-select').chameleonSelectList({ selectedValue: style.getFontSize() });
    populateAccounts();
    
    //----------------------------------------------------------------------
    // Functions
    //----------------------------------------------------------------------
    function populateStaticTextForLocale() {
        $('#account-fields label')._('drive_settings_account_label');
        $('#account-fields .help')._('drive_settings_account_help');
        $('#add-account-button')._('drive_settings_account_add_account');

        $('#layout-fields label')._('drive_settings_layout_label');
        $('label[for="show-details"]')._('drive_settings_showdetails_label');
        $('#fontsize-fields label')._('drive_settings_fontsize_label');

        $('#close-button')._('drive_settings_done');
    }

    function populateAccounts(accountToSelect) {
        if(accountToSelect === undefined) {
            accountToSelect = gapi.getAccount();
        }

        var availableAccounts = gapi.getAccounts();
        if(availableAccounts.length > 0) {
            $('#account-select').show();
            var options = [];
            for(var i = 0, account; account = availableAccounts[i]; i++) {
                options.push({
                    name: account.email,
                    value: account.id
                });
            }

            $('#account-select').chameleonSelectList({
                title: _('drive_settings_account_select_title'),
                list: options,
                changed: function(info) {
                    populateTasklists();
                },
                allow_delete: true,
                deleted: function(items) {
                    for(var i = 0, item; item = items[i]; i++) {
                        gapi.getAccount(item.value).remove();
                    }
                    
                    if(gapi.getAccounts().length == 0) {
                        $('#account-select').hide();
                    } else {
                        populateTasklists();
                    }
                }
            });
            
            if(accountToSelect !== undefined) {
                $('#account-select').chameleonSelectList({ selectedValue: accountToSelect.id });
            }
        }
    }
});