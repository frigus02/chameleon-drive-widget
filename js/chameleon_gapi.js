var gapi = (function() {
    var SCOPES = [
        'https://www.googleapis.com/auth/drive.readonly',
        'https://www.googleapis.com/auth/userinfo.email'
    ];
    var REDIRECT_URI = 'http://kuehle.me/chameleon/drive/services/oauth2.php';
    var CLIENT_ID = '868471416612.apps.googleusercontent.com';

    var me = {};
    
    me.addAccount = function(callback) {
        chameleon.promptOauth({
            version: '2.0',
            authorize: {
                url: 'https://accounts.google.com/o/oauth2/auth',
                args: {
                    response_type: 'code',
                    access_type: 'offline',
                    approval_prompt: 'force',
                    scope: SCOPES.join('+')
                }
            },
            callbackURL: REDIRECT_URI,
            consumerKey: CLIENT_ID,
            onResult: function(success, data) {
                if(success) {
                    var account = new Account(data);
                    account.save();
                    callback(account);
                }
            }
        });
    };

    me.getAccount = function(id) {
        // Get current account, when no id is specified.
        if(!id) {
            var data = chameleon.getData() || {};
            id = data.account;
        }

        var sharedData = chameleon.getSharedData() || {};
        if(!sharedData.accounts) sharedData.accounts = [];
        for(var i = 0, savedAccount; savedAccount = sharedData.accounts[i]; i++) {
            if(savedAccount.id == id) {
                return new Account(savedAccount);
            }
        }
    };

    me.getAccounts = function() {
        var sharedData = chameleon.getSharedData() || {};
        if(!sharedData.accounts) sharedData.accounts = [];
        
        // Remove accounts, that do not have an id.
        sharedData.accounts = $.grep(sharedData.accounts, function(n) {
            return n.id;
        });
        chameleon.saveSharedData(sharedData);

        // Return accounts.
        return $.map(sharedData.accounts, function(n, i) {
            return new Account(n);
        });
    };
    
    /**
    * @param {Object} args
    *   @param {String} args.path API path.
    *   @param {String} args.method Request method (GET, POST, PATCH, ...).
    *   @param {Object} [args.params] Get parameters.
    *   @param {Object} [args.body] Request body.
    *   @param {Object} [args.account] The account, that should be used to
    *     authorize this request. If no account is specified the current
    *     account is used.
    */
    me.Request = function(args) {
        // args.params and args.body
        args.params = args.params || {};
        if(args.body) {
            var urlParams = '?' + $.param(args.params);
            var params = JSON.stringify(args.body);
        } else {
            var urlParams = '';
            var params = args.params;
        }

        // There is a bug in Chameleon, which make it cache every GET request,
        // that does not have at least one parameter in the query string. So
        // if the urlParams are empty, we add a dummy argument to avoid this.
        if(urlParams == '') {
            urlParams = '?i=1';
        }

        // args.account
        args.account = args.account || me.getAccount();

        this.execute = function(callback) {
            args.account.execute(function() {
                $.ajax({
                    url: 'https://www.googleapis.com' + args.path + urlParams,
                    type: args.method,
                    contentType: 'application/json',
                    data: params,
                    dataType: 'json',
                    timeout: 5000,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Authorization', args.account.token_type + ' ' + args.account.access_token);
                    },
                    error: function() {
                        callback(false);
                    },
                    success: function(data) {
                        callback(true, data);
                    }
                });
            }, function() {
                callback(false);
            });
        };
    };

    me.Batch = function() {
        var requests = {};
        var responses = {};
        
        this.add = function(id, request) {
            requests[id] = request;
        };
            
        this.execute = function(callback) {
            for(var id in requests) {
                requests[id].execute((function(requestID) {
                    return function(success, data) {
                        responses[requestID] = {
                            success: success,
                            data: data
                        };

                        var allResponsesReceived = true;
                        for(var id in requests) {
                            if(!responses[id]) {
                                allResponsesReceived = false;
                                break;
                            }
                        }
                        
                        if(allResponsesReceived) {
                            callback(responses);
                        }
                    };
                })(id));
            }
        };
    };

    function Account(data) {
        this.id = data.id;
        this.access_token = data.access_token;
        this.token_type = data.token_type;
        this.created = data.created;
        this.expires_in = data.expires_in;
        this.refresh_token = data.refresh_token;
        this.email = data.email || 'unknown';

        if(data.id_token) {
            var token_parts = data.id_token.split('.'),
                header = JSON.parse(atob(token_parts[0])),
                claims = JSON.parse(atob(token_parts[1])),
                signature = token_parts[2];
            this.id = claims.id;
            this.email = claims.email || 'unknown';
        }
        
        this.execute = function(func, error) {
            var expireDate = (parseInt(this.created) + parseInt(this.expires_in)) * 1000;
            var buffer = 10 * 1000;
            if(expireDate > new Date().getTime() + buffer) {
                func();
            } else {
                var self = this;
                $.ajaxSetup({ timeout: 5000 });
                $.getJSON('services/oauth2.php', { refresh_token: this.refresh_token }, function(data) {    
                    self.access_token = data.access_token;
                    self.created = data.created;
                    self.expires_in = data.expires_in;
                    self.save();
                    func();
                }).error(function(a, b, c) {
                    if(error) error();
                });
            }
        };

        this.remove = function() {
            // Remove account from the shared data.
            var sharedData = chameleon.getSharedData() || {};
            if(!sharedData.accounts) sharedData.accounts = [];

            self = this;
            sharedData.accounts = $.grep(sharedData.accounts, function(n) {
                return n.id != self.id;
            });

            chameleon.saveSharedData(sharedData);

            // If this is the current account, set current to undefined.
            var data = chameleon.getData() || {};
            if(data.account == this.id) {
                delete data.account;
                chameleon.saveData(data);
            }

            // Revoke access to the account.
            $.get('https://accounts.google.com/o/oauth2/revoke', { token: this.refresh_token });
        };
        
        this.save = function() {
            var saveData = {};
            for(var prop in this) {
                if(typeof this[prop] != 'function') {
                    saveData[prop] = this[prop];
                }
            }
        
            var sharedData = chameleon.getSharedData() || {};
            if(!sharedData.accounts) sharedData.accounts = [];
            
            var found = false;
            for(var i = 0, savedAccount; savedAccount = sharedData.accounts[i]; i++) {
                if(savedAccount.id == this.id) {
                    $.extend(savedAccount, saveData);
                    found = true;
                }
            }
            if(!found) sharedData.accounts.push(saveData);
            
            chameleon.saveSharedData(sharedData);
        };

        this.setAsCurrent = function() {
            var data = chameleon.getData() || {};
            data.account = this.id;
            chameleon.saveData(data);
        };
    }

    return me;
})();