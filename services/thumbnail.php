<?php

header('Content-Type: image/png');

$context = stream_context_create(array(
    'http' => array(
        'method' => "GET",
        'header' => "Accept: image/png\r\n" .
                    "Accept-Language: en\r\n" .
                    "Accept-Encoding: gzip, deflate\r\n" .
                    "Connection: close\r\n" .
                    "Authorization: " . $_GET['token_type'] . " " . $_GET['access_token'] . "\r\n"
    )
));

echo file_get_contents($_GET['url'], false, $context);

?>